<?php


namespace Chenhaitry\Permission\Providers;


use Illuminate\Support\ServiceProvider as ServiceProvider;

class MigrationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return false
     */
    public function boot()
    {

        //Publishing Migrations
        if ($this->app->runningInConsole()) {
            $this->publishes([
                $this->getMigrationPath() => database_path('migrations'),
            ], 'laravel-role-permission-migrations');
        }

        // Loading Migrations Automatically
//        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

    }


    /**
     * Migration Path.
     *
     * @return string
     */
    protected function getMigrationPath()
    {
        return __DIR__ .'/../../database/migrations';
    }
}