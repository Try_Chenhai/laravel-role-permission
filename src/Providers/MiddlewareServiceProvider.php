<?php


namespace Chenhaitry\Permission\Providers;

use Chenhaitry\Permission\Http\Middleware\PermissionMiddleware;
use Chenhaitry\Permission\Http\Middleware\RoleMiddleware;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as ServiceProvider;

class MiddlewareServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return false
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function boot()
    {

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('role', RoleMiddleware::class);
        $router->aliasMiddleware('permission', PermissionMiddleware::class);

    }


}