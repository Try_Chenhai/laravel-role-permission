<?php


namespace Chenhaitry\Permission\Traits;

use Chenhaitry\Permission\Models\Role;

trait HasRoles
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Role::class,'user_roles','user_id','role_id');
    }

    /**
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(... $roles): bool
    {
        foreach ($roles as $role) {
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }
        return false;
    }
}
