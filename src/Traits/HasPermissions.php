<?php


namespace Chenhaitry\Permission\Traits;

use Chenhaitry\Permission\Models\Permission;

trait HasPermissions
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Permission::class,'user_permissions','user_id','permission_id')
            ->withPivot('access','read','create','update','delete','restore');
    }

    /**
     * @param $permission
     * @param null $action
     * @return bool
     */
    public function hasPermission($permission,$action=null): bool
    {
        $has_permission = $this->permissions()->whereSlug($permission)->first();
        if ($action!=null || $action!='') {
            if ($has_permission && $has_permission->pivot->{$action}===1) {
                return true;
            }
            return false;
        }
        return (bool) ($has_permission?$has_permission->count():0);
    }

    /**
     * @param $permission
     * @param null $action
     * @return bool
     */
    public function hasPermissionThroughRole($permission,$action=null): bool
    {
        $has_permission = Permission::whereSlug($permission)->first();
        if ($has_permission) {
           foreach ($has_permission->roles as $role){
               if($this->roles->contains($role)) {
                   $has_role_permission = $role->permissions()->whereSlug($permission)->first();
                   if ($has_role_permission) {
                       if ($action!=null || $action!='') {
                           return (bool) $has_role_permission->pivot->{$action};
                       }
                       return true;
                   }
               }
           }
        }
        return false;
    }

    /**
     * @param $permission
     * @param null $action
     * @return bool
     */
    public function hasPermissionTo($permission,$action=null): bool
    {
        return $this->hasPermissionThroughRole($permission,$action) || $this->hasPermission($permission,$action);
    }

    /**
     * @param array $permissions
     */
    protected function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug',$permissions)->get();
    }

    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function givePermissionsTo(... $permissions): HasPermissions
    {
        $permissions = $this->getAllPermissions($permissions);
        if($permissions === null) {
            return $this;
        }
        $this->permissions()->saveMany($permissions);
        return $this;
    }

    /**
     * @param mixed ...$permissions
     * @return $this
     */
    public function deletePermissions(... $permissions ): HasPermissions
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);
        return $this;
    }

    /**
     * @param mixed ...$permissions
     * @return HasPermissions
     */
    public function refreshPermissions(... $permissions ): HasPermissions
    {
        $this->permissions()->detach();
        return $this->givePermissionsTo($permissions);
    }

}
