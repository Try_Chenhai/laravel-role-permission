<?php

namespace Chenhaitry\Permission\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'description',
        'active',
        'delete',
    ];
}
