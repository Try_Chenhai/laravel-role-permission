<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_permissions', function (Blueprint $table) {
            $table->foreignId('role_id')->references('id')
                ->on('roles')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('permission_id')->references('id')
                ->on('permissions')->cascadeOnDelete()->cascadeOnUpdate();
            $table->boolean('access')->default(0)->comment('0=Cannot access, 1=Access');
            $table->boolean('read')->default(0)->comment('0=Cannot Read/View, 1=Read/View');
            $table->boolean('create')->default(0)->comment('0=Cannot Create, 1=Create');
            $table->boolean('update')->default(0)->comment('0=Cannot Update, 1=Update');
            $table->boolean('delete')->default(0)->comment('0=Cannot Delete, 1=Delete');
            $table->boolean('restore')->default(0)->comment('0=Cannot Restore, 1=Restore');
            $table->timestamps();
            $table->primary([
                'role_id',
                'permission_id'
            ],'role_permissions_pk_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_permissions');
    }
}
