<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_permissions', function (Blueprint $table) {
            $table->foreignId('user_id')->references('id')
                ->on('users')->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignId('permission_id')->references('id')
                ->on('permission')->cascadeOnDelete()->cascadeOnUpdate();
            $table->boolean('access')->default(0)->comment('0=Cannot access, 1=Access');
            $table->boolean('read')->default(0)->comment('0=Cannot Read/View, 1=Read/View');
            $table->boolean('create')->default(0)->comment('0=Cannot Create, 1=Create');
            $table->boolean('update')->default(0)->comment('0=Cannot Update, 1=Update');
            $table->boolean('delete')->default(0)->comment('0=Cannot Delete, 1=Delete');
            $table->boolean('restore')->default(0)->comment('0=Cannot Restore, 1=Restore');
            $table->timestamps();
            $table->primary(['user_id','permission_id'],'user_permissions_pk_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_permissions');
    }
}
