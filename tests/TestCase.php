<?php

namespace JohnDoe\BlogPackage\Tests;

use Chenhaitry\Permission\Providers\MigrationServiceProvider;
use Chenhaitry\Permission\Providers\PermissionServiceProvider;
use Chenhaitry\Permission\Providers\RoleServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            MigrationServiceProvider::class,
            PermissionServiceProvider::class,
            RoleServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}